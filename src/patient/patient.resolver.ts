import {
  Args,
  Int,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';

import { Patient } from './patient.entity';
import { CreatePatientDto } from './patient.dto';
import { PatientService } from './patient.service';
import { Appointment } from 'src/appointment/appointment.entity';
import { MailService } from '../mail/mail.service';

@Resolver(() => Patient)
export class PatientResolver {
  constructor(
    private readonly patientService: PatientService,
    private readonly mailService: MailService,
  ) {}

  @ResolveField('appointments', () => [Appointment])
  async appointments(@Parent() patient: Patient) {
    const { id } = patient;
    return this.patientService.getAppointments(id);
  }

  @Query(() => [Patient])
  async patients(): Promise<Patient[]> {
    return this.patientService.getAll();
  }

  @Query(() => Patient)
  async patient(@Args('id', { type: () => Int }) id: number): Promise<Patient> {
    return this.patientService.getPatientById(id);
  }

  @Mutation(() => Patient)
  async registerPatient(@Args('input') input: CreatePatientDto) {
    const newPatient: Patient = await this.patientService.create(input);
    if (newPatient) {
      try {
        this.mailService.registeredPatient(
          newPatient.name,
          newPatient.lastName,
          newPatient.email,
        );
      } catch (error) {
        console.log('Error sending registration email:', error);
      }
    }
    return newPatient;
  }
}
